package com.danusin.registration.controller;

import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/register")
    public String registrationPage(User user){
        return "registration-page";
    }

    @GetMapping("/profile-user")
    public String createUser(Model model, Principal principal){
        String username = principal.getName();
        model.addAttribute("user", userService.findByUsername(username));
        return "index";
    }

    @PostMapping("/create-user")
    public String createUser(User user, Model model){
        userService.createUser(user);

        return "redirect:/login";
    }
}
