package com.danusin.registration;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RegistrationApplicationTests.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BaseIntegrationTest {

    @Test
    public void none(){
    }
}
