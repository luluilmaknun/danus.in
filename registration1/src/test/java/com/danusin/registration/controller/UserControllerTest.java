package com.danusin.registration.controller;

import com.danusin.registration.BaseIntegrationTest;
import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.service.UserService;
import com.danusin.registration.service.implement.UserServiceImplement;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

//@RunWith(SpringRunner.class)
//@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
public class UserControllerTest extends BaseIntegrationTest {
    private User user;
    private Model model;

    @Autowired
    UserService userService;

    @Autowired
    private MockMvc mockMvc;

//    @Before
//    public void setUp() throws Exception {
//        this.mockMvc = standaloneSetup(new UserController()).build();
//
//        user = new User();
//        user.setRole("Danuser");
//        user.setPassword("adpro_ganteng2019");
//        user.setUsername("adpro_ganteng");
//    }

    @Test
    public void registrationPage() throws Exception{
        mockMvc.perform(get("/register")).andExpect(status().isFound());
    }

    @Test
    public void createUser() throws Exception{
        mockMvc.perform(get("/create-user")).andExpect(status().isFound());
    }
}