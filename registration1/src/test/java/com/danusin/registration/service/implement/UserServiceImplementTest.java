package com.danusin.registration.service.implement;

import com.danusin.registration.BaseIntegrationTest;
import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.vo.AuthVo;
import com.danusin.registration.vo.ResponseVo;
import com.danusin.registration.vo.UserVo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

@Transactional
public class UserServiceImplementTest extends BaseIntegrationTest {
    @Autowired
    private UserServiceImplement userServiceImplement;

    private UserVo vo = new UserVo();
    private User user = new User();

    @Before
    public void before(){
        vo.setUsername("test");
        vo.setName("name test");
        vo.setPassword("test123");
        vo.setPhoneNumber("08080808");
        vo.setCompany("company test");

        user.setUsername("test");
        user.setName("name test");
        user.setPassword("test123");
        user.setPhoneNumber("08080808");
        user.setCompany("company test");
    }

    @Test
    public void a1_createUser_success(){
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNotNull(responseVo);
        Assert.assertTrue(responseVo.isSuccess());
    }

    @Test
    public void a2_createUser_failOnNullVo(){
        vo = null;
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNull(responseVo);
    }

    @Test
    public void a3_createUser_failOnNullUsername(){
        vo.setUsername(null);
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNull(responseVo);
    }

    @Test
    public void a4_createUser_failOnNullPassword(){
        vo.setPassword(null);
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNull(responseVo);
    }

    @Test
    public void a5_createUser_failOnEmptyUsername(){
        vo.setUsername("");
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNull(responseVo);
    }

    @Test
    public void a6_createUser_failOnEmptyPassword(){
        vo.setPassword("");
        ResponseVo responseVo = userServiceImplement.createUser(vo);
        Assert.assertNull(responseVo);
    }

    @Test
    public void b1_restLogin_success(){
        userServiceImplement.createUser(vo);
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), vo.getPassword());
        Assert.assertNotNull(authVo);
    }

    @Test
    public void b2_restLogin_failOnNullUsername(){
        AuthVo authVo = userServiceImplement
                .restLogin(null, vo.getPassword());
        Assert.assertNull(authVo);
    }

    @Test
    public void b3_restLogin_failOnNullPassword(){
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), null);
        Assert.assertNull(authVo);
    }

    @Test
    public void b4_restLogin_failOnEmptyUsername(){
        AuthVo authVo = userServiceImplement
                .restLogin("", vo.getPassword());
        Assert.assertNull(authVo);
    }

    @Test
    public void b5_restLogin_failOnEmptyPassword(){
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), "");
        Assert.assertNull(authVo);
    }

    @Test
    public void b6_restLogin_failOnUnExistUser(){
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), vo.getPassword());
        Assert.assertNull(authVo);
    }

    @Test
    public void c1_restLogout_success(){
        userServiceImplement.createUser(vo);
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), vo.getPassword());
        ResponseVo responseVo = userServiceImplement
                .restLogout(authVo.getAuthString());
        Assert.assertNotNull(responseVo);
        Assert.assertTrue(responseVo.isSuccess());
    }

    @Test
    public void c2_restLogout_failOnNullAuthString(){
        ResponseVo responseVo = userServiceImplement
                .restLogout(null);
        Assert.assertNull(responseVo);
    }

    @Test
    public void c3_restLogout_failOnEmptyAuthString(){
        ResponseVo responseVo = userServiceImplement
                .restLogout("");
        Assert.assertNull(responseVo);
    }

    @Test
    public void c4_restLogout_failOnWrongAuthStringFormat(){
        ResponseVo responseVo = userServiceImplement
                .restLogout("wrongAuthStringFormat");
        Assert.assertNull(responseVo);
    }

    @Test
    public void c5_restLogout_failOnUnExistUser(){
        ResponseVo responseVo = userServiceImplement
                .restLogout("unExistUser===unExistPassword");
        Assert.assertNull(responseVo);
    }

    @Test
    public void c6_restLogout_failOnWrongPassword(){
        userServiceImplement.createUser(vo);
        ResponseVo responseVo = userServiceImplement
                .restLogout("test===unExistPassword");
        Assert.assertNull(responseVo);
    }

    @Test
    public void d1_isAuth_success(){
        userServiceImplement.createUser(vo);
        AuthVo authVo = userServiceImplement
                .restLogin(vo.getUsername(), vo.getPassword());
        UserVo userVo = userServiceImplement
                .isAuth(authVo.getAuthString());
        Assert.assertNotNull(userVo);
        Assert.assertEquals(vo.getUsername(), userVo.getUsername());
        Assert.assertEquals(vo.getName(), userVo.getName());
        Assert.assertEquals(vo.getCompany(), userVo.getCompany());
        Assert.assertEquals(vo.getPhoneNumber(), userVo.getPhoneNumber());
    }

    @Test
    public void d2_isAuth_failOnNullAuthString(){
        UserVo userVo = userServiceImplement
                .isAuth(null);
        Assert.assertNull(userVo);
    }

    @Test
    public void d3_isAuth_failOnEmptyAuthString(){
        UserVo userVo = userServiceImplement
                .isAuth("");
        Assert.assertNull(userVo);
    }

    @Test
    public void d4_isAuth_failOnWrongAuthStringFormat(){
        UserVo userVo = userServiceImplement
                .isAuth("wrongAuthStringFormat");
        Assert.assertNull(userVo);
    }

    @Test
    public void d5_isAuth_failOnUnExistUser(){
        UserVo userVo = userServiceImplement
                .isAuth("unExistUser===unExistPassword");
        Assert.assertNull(userVo);
    }

    @Test
    public void d6_isAuth_failOnWrongPassword(){
        userServiceImplement.createUser(vo);
        UserVo userVo = userServiceImplement
                .isAuth("test===unExistPassword");
        Assert.assertNull(userVo);
    }

}