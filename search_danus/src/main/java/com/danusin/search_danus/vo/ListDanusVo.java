package com.danusin.search_danus.vo;

import com.danusin.search_danus.domain.persistence.MenuModel;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collection;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListDanusVo {

    private Collection<MenuModel> listMenu;

    public Collection<MenuModel> getListMenu() {
        return listMenu;
    }

    public void setListMenu(Collection<MenuModel> listMenu) {
        this.listMenu = listMenu;
    }


}
