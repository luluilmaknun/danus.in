package com.danusin.search_danus.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.danusin.search_danus.domain.persistence.VendorModel;

@Repository
public interface VendorDb extends JpaRepository<VendorModel, Integer>{
    VendorModel findByNamaVendor(String namaVendor);
}