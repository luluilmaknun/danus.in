package com.danusin.search_danus.domain.persistence;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

@Entity
@Table(name="menu")
public class MenuModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(max=50)
    @Column(name="namaMakanan", nullable= false)
    private String namaMakanan;

    @NotNull
    @Column(name="harga")
    private int harga;

    @Size(max=70)
    @Column(name="detailMakanan", nullable= true)
    private String detailMakanan;

    //Fk to vendor
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name= "namaVendor", referencedColumnName= "id", nullable = true)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private VendorModel namaVendor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNamaMakanan() {
        return namaMakanan;
    }

    public void setNamaMakanan(String namaMakanan) {
        this.namaMakanan = namaMakanan;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getDetailMakanan() {
        return detailMakanan;
    }

    public void setDetailMakanan(String detailMakanan) {
        this.detailMakanan = detailMakanan;
    }

    public VendorModel getNamaVendor() {
        return namaVendor;
    }

    public void setNamaVendor(VendorModel namaVendor) {
        this.namaVendor = namaVendor;
    }
}