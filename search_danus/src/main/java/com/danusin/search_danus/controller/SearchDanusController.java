package com.danusin.search_danus.controller;

import com.danusin.search_danus.domain.persistence.MenuModel;
import com.danusin.search_danus.domain.persistence.VendorModel;
import com.danusin.search_danus.service.MenuService;
import com.danusin.search_danus.service.VendorService;
import com.danusin.search_danus.vo.ListDanusVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchDanusController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private VendorService vendorService;

    private ListDanusVo danusVo = new ListDanusVo();

    public void initServicesManual() {
        VendorModel vendor1 = new VendorModel();
        vendor1.setNamaVendor("Pudding dan Cheese Stick Alsa");
        vendor1.setTelepon("087875118600");

        VendorModel vendor2 = new VendorModel();
        vendor2.setNamaVendor("Martabak Martha Tilaar");
        vendor2.setTelepon("14045");

        vendorService.createVendor(vendor1);
        vendorService.createVendor(vendor2);

        MenuModel menu1 = new MenuModel();
        menu1.setNamaMakanan("Pudding Coklat");
        menu1.setDetailMakanan("Pudding rasa coklat pakai whip cream dan taburan coklat");
        menu1.setHarga(5000);
        menu1.setNamaVendor(vendorService.findByNamaVendor(vendor1.getNamaVendor()));


        MenuModel menu2 = new MenuModel();
        menu2.setNamaMakanan("Cheese Stick");
        menu2.setDetailMakanan("Stik keju 1 bungkus untuk ngemil");
        menu2.setHarga(10000);
        menu2.setNamaVendor(vendorService.findByNamaVendor(vendor1.getNamaVendor()));


        MenuModel menu3 = new MenuModel();
        menu3.setNamaMakanan("Martaak");
        menu3.setDetailMakanan("Marta(b)ak asin gapake bawang");
        menu3.setHarga(30000);
        menu3.setNamaVendor(vendorService.findByNamaVendor(vendor2.getNamaVendor()));

        menuService.createMenu(menu1);
        menuService.createMenu(menu2);
        menuService.createMenu(menu3);
    }

    @GetMapping(value = "/menus", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ListDanusVo getDanusMenus(){
        try {
            initServicesManual();
            danusVo.setListMenu(menuService.getListMenu());
            return danusVo;
        } catch (Exception e) {
            return danusVo;
        }
    }
}
