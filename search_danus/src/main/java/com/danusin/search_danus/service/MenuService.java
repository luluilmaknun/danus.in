package com.danusin.search_danus.service;

import com.danusin.search_danus.domain.persistence.MenuModel;

import java.util.Collection;

public interface MenuService {
    MenuModel createMenu(MenuModel vendorModel);
    Collection<MenuModel> getListMenu();
    MenuModel findByNamaMakanan(String namaMakanan);
}
