package com.danusin.search_danus.service;

import com.danusin.search_danus.domain.persistence.VendorModel;

import java.util.Collection;

public interface VendorService {
    VendorModel createVendor(VendorModel vendorModel);
    Collection<VendorModel> getListVendor();
    VendorModel findByNamaVendor(String namaVendor);
}
