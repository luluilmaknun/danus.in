package com.danusin.user_interface.model;

public class User {

    private String username;
    private String name;
    private String password;
    private String institusi;
    private String phone;

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getInstitusi() {
        return institusi;
    }

    public String getPhone() {
        return phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setInstitusi(String institusi) {
        this.institusi = institusi;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }
}
