package com.danusin.user_interface.controller;

import com.danusin.user_interface.model.User;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UIController {

    @Value("${registration.url}")
    private String url;

    String auth;
    JSONObject jsonObject;
    User user;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    public String openWelcomePage(Model model) {
        if ( user != null ) {
            model.addAttribute("name",user.getName());
            return "dashboard";
        }
        model.addAttribute("user", new User());
        return "welcome-page";
    }

    @PostMapping("log-in")
    public String getAuthFromRegisApp(@ModelAttribute User user, Model model) throws Exception {
        this.user = user;
        String urlAuth = url + "rest-login?username=" + user.getUsername() + "&password=" + user.getPassword();
        logger.info(postJSONResponseToOtherApp(urlAuth));
        jsonObject = new JSONObject(postJSONResponseToOtherApp(urlAuth));
        auth = (String) jsonObject.get("authString");
        String urlSuccess = url + "is-auth?auth-string=" + auth;
        jsonObject = new JSONObject(getJSONResponseFromOtherApp(urlSuccess));

        user.setName((String) jsonObject.get("name"));
        user.setInstitusi((String) jsonObject.get("company"));
        user.setPhone((String) jsonObject.get("phoneNumber"));

        model.addAttribute("name", user.getName());
        return "dashboard";
    }

    @GetMapping("/profile")
    public String getProfile(Model model) {
        model.addAttribute("user",user);
        return "profile";
    }

    @GetMapping("/search")
    public String getSearchDanusPage(Model model) throws Exception {
        String urlSearch = "https://danus-in3.herokuapp.com/menus";
        jsonObject = new JSONObject(getJSONResponseFromOtherApp(urlSearch));
        JSONArray jsonArray = jsonObject.getJSONArray("listMenu");
        model.addAttribute("menus",jsonArray);
        return "danus";
    }

    @PostMapping("/log-out")
    public String loggingOut(Model model) throws Exception {
        String urlOut = url + "rest-logout?auth-string=" + auth;
        jsonObject = new JSONObject(postJSONResponseToOtherApp(urlOut));
        user = null;
        return "logout";
    }

    public String postJSONResponseToOtherApp(String url) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        return httpclient.execute(httpPost, createResponseHandler());
    }

    public String getJSONResponseFromOtherApp(String url) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        //HTTP GET method
        HttpGet httpget = new HttpGet(url);
        return httpclient.execute(httpget, createResponseHandler());
    }

    public ResponseHandler<String> createResponseHandler() {
        ResponseHandler<String> responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            } };
        return responseHandler;
    }

}
