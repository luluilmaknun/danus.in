package com.danusin.user_interface.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    private User user;

    @Before
    public void setUp() {
        user = new User();
        user.setName("Test");
        user.setPhone("021");
        user.setPassword("asdfgh");
        user.setUsername("Haduh");
        user.setInstitusi("UEH");
    }

    @Test
    public void testGetName() {
        assertNotNull(user.getName());
    }

    @Test
    public void testGetPhone() {
        assertNotNull(user.getPhone());
    }

    @Test
    public void testGetUsername() {
        assertNotNull(user.getUsername());
    }

    @Test
    public void testGetPassword() {
        assertNotNull(user.getPassword());
    }

    @Test
    public void testGetInstitute() {
        assertNotNull(user.getInstitusi());
    }
}
