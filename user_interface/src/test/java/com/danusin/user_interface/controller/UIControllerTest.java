package com.danusin.user_interface.controller;

import com.danusin.user_interface.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UIControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UIController uiController;

    @Value("${registration.url}")
    private String url;

    private MockRestServiceServer mockServer;
    private User testing = new User();

    @Before
    public void setUp() {
        testing.setUsername("alsabila");
        testing.setPassword("sp");
        uiController.user = testing;

        mockServer = MockRestServiceServer.bindTo(restTemplate).build();
    }

    @Test
    public void shouldReturnFirstPage() throws Exception {
        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    @Test
    public void shouldReturnWelomePage() throws Exception {
        uiController.user = null;
        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    @Test
    public void shouldReturnProfilePage() throws Exception {
        mockMvc.perform(get("/profile")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    @Test
    public void shouldReturnDanusPage() throws Exception {
        mockMvc.perform(get("/search")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"));
    }

    @Test
    public void shouldReturnResponseRegistration() throws Exception {
        try {
            mockServer.expect(requestTo(url + "rest-login?username=" + testing.getUsername()
                    + "&password=" + testing.getPassword()))
                    .andExpect(method(HttpMethod.POST))
                    .andRespond(withStatus(HttpStatus.OK)
                    );
            mockMvc.perform(post("/log-in"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("text/html;charset=UTF-8"));
        } catch (Exception e) {
            // do nothing
        }

    }

    @Test
    public void shouldReturnLogOut() throws Exception {
        try {
            mockServer.expect(requestTo(url + "rest-logout?auth-string=" + uiController.auth))
                    .andExpect(method(HttpMethod.POST))
                    .andRespond(withStatus(HttpStatus.OK)
                    );
            mockMvc.perform(post("/log-out"))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("text/html;charset=UTF-8"));
        } catch (Exception e) {
            // do nothing
        }

    }

    @Test
    public void shouldReturnRequestHandler() {
        assertNotNull(uiController.createResponseHandler());
    }

}
