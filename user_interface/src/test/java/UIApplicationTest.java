import com.danusin.user_interface.UIApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UIApplication.class)
public class UIApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void app_Context_Ok() {
        UIApplication.main(new String[] {});
    }


}
